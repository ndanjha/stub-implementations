import { getImagesResponse, postImageResponse } from "../apiStubs"

export function getImages () {
return new Promise ((resolve, reject)=>{
    resolve(getImagesResponse)
})
}
export const postImage = formData => {
    return new Promise ((resolve, reject) => {
        resolve (postImageResponse);
        setTimeout(() => {
            reject (new Error("Failed to upload image, please try again!"));
        }, 5000);
    });
};
