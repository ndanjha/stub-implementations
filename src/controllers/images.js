import {getImagesResponse, postImageResponse} from "../apiStubs";

export const getImages = (request, response) => {
    console.log("getImages");
    response.send(getImagesResponse);
};

export const postImage = (request, response) => {
    console.log("postImage");
    response.send(postImageResponse);
};